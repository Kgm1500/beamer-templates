#!/usr/bin/env perl

$our_dir = 'output';
$pvc_view_file_via_temporary = 0;

if ($^O eq 'MSWin32') {
  $latex = 'uplatex %O -kanji=utf8 -no-guess-input-enc -synctex=1 -interaction=nonstopmode -file-line-error %S';
  $pdflatex = 'pdflatex %O -synctex=1 -interaction=nonstopmode -file-line-error %S';
  $bibtex = 'upbibtex %O %B';
  $dvipdf = 'dvipdfmx %O -o %D %S';
} else {
  $latex = 'uplatex %O -synctex=1 -interaction=nonstopmode -file-line-error %S';
  $pdflatex = 'pdflatex %O -synctex=1 -interaction=nonstopmode -file-line-error %S';
  $bibtex = 'pbibtex %O %B';
  $dvipdf = 'dvipdfmx %O -o %D %S';
}